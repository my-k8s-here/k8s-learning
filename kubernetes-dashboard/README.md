# Установка
- Установить istio одним из способов, указанных по ссылке https://cloud.yandex.ru/ru/docs/managed-kubernetes/operations/applications/istio
- Загрузить файлы из репозитория https://gitlab.com/my-k8s-here/k8s-learning/-/tree/main/kubernetes-dashboard?ref_type=heads
- Добавить доменную зону devops.com. ( public)
- Добавить А-запись dashboard.devops.com , которая ссылается на балансировщик
- Выполнить миграцию в kubernetes `kubectl apply -f kubernetes-dashboard/.`
